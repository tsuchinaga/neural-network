package simple_perceptron

import (
	"errors"
	"math/rand"
)

var (
	notMatchNumberOfInputsError = errors.New("入力ノード数と入力値の数が一致しません")
)

// NewSP - 新しい単純パーセプトロンの構造体を生成
func NewSP(input int, h float64, seed int64) *SP {
	r := rand.New(rand.NewSource(seed))

	w := make([]float64, input+1) // バイアス項を追加
	for i := 0; i < input+1; i++ {
		w[i] = r.Float64()
	}

	return &SP{
		input:  input,
		h:      h,
		weight: w,
	}
}

// SP - 単純パーセプトロン(simple perceptron)
type SP struct {
	input  int       // 入力層の数
	weight []float64 // 入力層から出力層に伝えられる重み
	h      float64   // 学習係数
}

// Output - 入力に対しての判断
func (sp *SP) Output(inputs []float64) (float64, error) {
	if len(inputs) != sp.input {
		return -1, notMatchNumberOfInputsError
	}

	sum := 0.0
	inputs = append(inputs, 1) // バイアス項に与えるデータを追加
	for i := range sp.weight {
		sum += sp.weight[i] * inputs[i]
	}

	// 閾値0で、0より大きな値なら1、そうでないなら0
	return sp.A(sum), nil
}

// Learn - 入力と正答をもらって重みを更新する
func (sp *SP) Learn(inputs []float64, answer float64) error {
	if len(inputs) != sp.input {
		return notMatchNumberOfInputsError
	}

	// spの回答を取得
	output, err := sp.Output(inputs)
	if err != nil {
		return err
	}

	d := sp.E(output, answer)
	inputs = append(inputs, 1) // バイアス項に与えるデータを追加
	for i := range sp.weight {
		sp.weight[i] = sp.weight[i] + inputs[i]*d*sp.h
	}

	return nil
}

// E - 誤差関数(error function)。出力と正答の誤差を返す
func (sp *SP) E(output float64, answer float64) float64 {
	return answer - output
}

// A - 活性化関数(activation function)
func (sp *SP) A(output float64) float64 {
	if output > 0 {
		return 1
	}
	return 0
}
