package simple_perceptron

import (
	"errors"
	"reflect"
	"testing"
	"time"
)

func TestNewSP(t *testing.T) {
	t.Parallel()

	testTable := []struct {
		desc   string
		input  int
		h      float64
		seed   int64
		expect *SP
	}{
		{desc: "inputが0でも動くけど、役に立たない構造体が作られる", input: 0, h: 0.1, seed: -1, expect: &SP{input: 0, h: 0.1, weight: []float64{0.3951876009960174}}},
		{desc: "inputが1なら重みをもつ項目も1つ作られる", input: 1, h: 0.1, seed: -1, expect: &SP{input: 1, h: 0.1, weight: []float64{0.3951876009960174, 0.05964969860952545}}},
		{desc: "inputが3なら重みをもつ項目も3つ作られる", input: 3, h: 0.1, seed: -1, expect: &SP{input: 3, h: 0.1, weight: []float64{0.3951876009960174, 0.05964969860952545, 0.33545830491955936, 0.5622259697326123}}},
		{desc: "seedを変えたら初期の重みも変わる", input: 3, h: 0.1, seed: -2, expect: &SP{input: 3, h: 0.1, weight: []float64{0.8328240056498365, 0.7347809470961395, 0.9484607146148828, 0.8806961955250229}}},
	}

	for _, test := range testTable {
		test := test
		t.Run(test.desc, func(t *testing.T) {
			t.Parallel()
			actual := NewSP(test.input, test.h, test.seed)
			if !reflect.DeepEqual(test.expect, actual) {
				t.Fatalf("%s 失敗\n期待: %+v\n実際: %+v\n", t.Name(), test.expect, actual)
			}
		})
	}
}

func TestSP_Output(t *testing.T) {
	t.Parallel()
	testTable := []struct {
		desc    string
		sp      *SP
		inputs  []float64
		expect1 float64
		expect2 error
	}{
		{desc: "inputsの数が合わなければエラー", sp: &SP{input: 1}, inputs: []float64{}, expect1: -1, expect2: notMatchNumberOfInputsError},
		{desc: "バイアス項だけなら重みが0より大きければ1が返される", sp: &SP{input: 0, weight: []float64{0.1}}, inputs: []float64{}, expect1: 1},
		{desc: "バイアス項だけなら重みが0以下なら0が返される", sp: &SP{input: 0, weight: []float64{0}}, inputs: []float64{}, expect1: 0},
		{desc: "バイアス項の重みが0なら、inputsの重み(1)だけで判断できる", sp: &SP{input: 1, weight: []float64{0.1, 0}}, inputs: []float64{1}, expect1: 1},
		{desc: "バイアス項の重みが0なら、inputsの重み(0)だけで判断できる", sp: &SP{input: 1, weight: []float64{0.1, 0}}, inputs: []float64{0}, expect1: 0},
		{desc: "AND回路 0,0 = 0", sp: &SP{input: 2, weight: []float64{0.5, 0.5, -0.5}}, inputs: []float64{0, 0}, expect1: 0},
		{desc: "AND回路 1,0 = 0", sp: &SP{input: 2, weight: []float64{0.5, 0.5, -0.5}}, inputs: []float64{1, 0}, expect1: 0},
		{desc: "AND回路 0,1 = 0", sp: &SP{input: 2, weight: []float64{0.5, 0.5, -0.5}}, inputs: []float64{0, 1}, expect1: 0},
		{desc: "AND回路 1,1 = 1", sp: &SP{input: 2, weight: []float64{0.5, 0.5, -0.5}}, inputs: []float64{1, 1}, expect1: 1},
		{desc: "OR回路 0,0 = 0", sp: &SP{input: 2, weight: []float64{0.1, 0.4, 0.0}}, inputs: []float64{0, 0}, expect1: 0},
		{desc: "OR回路 1,0 = 1", sp: &SP{input: 2, weight: []float64{0.1, 0.4, 0.0}}, inputs: []float64{1, 0}, expect1: 1},
		{desc: "OR回路 0,1 = 1", sp: &SP{input: 2, weight: []float64{0.1, 0.4, 0.0}}, inputs: []float64{0, 1}, expect1: 1},
		{desc: "OR回路 1,1 = 1", sp: &SP{input: 2, weight: []float64{0.1, 0.4, 0.0}}, inputs: []float64{1, 1}, expect1: 1},
	}

	for _, test := range testTable {
		test := test
		t.Run(test.desc, func(t *testing.T) {
			t.Parallel()

			actual1, actual2 := test.sp.Output(test.inputs)
			if !reflect.DeepEqual(test.expect1, actual1) || !errors.Is(actual2, test.expect2) {
				t.Fatalf("%s 失敗\n期待: %+v, %v\n実際: %+v, %v\n", t.Name(),
					test.expect1, test.expect2, actual1, actual2)
			}
		})
	}
}

func TestSP_Learn(t *testing.T) {
	t.Parallel()

	type train struct {
		inputs []float64
		answer float64
	}
	testTable := []struct {
		desc   string
		trains []train
	}{
		{desc: "AND回路", trains: []train{{inputs: []float64{0, 0}, answer: 0}, {inputs: []float64{1, 0}, answer: 0}, {inputs: []float64{0, 1}, answer: 0}, {inputs: []float64{1, 1}, answer: 1}}},
		{desc: "OR回路", trains: []train{{inputs: []float64{0, 0}, answer: 0}, {inputs: []float64{1, 0}, answer: 1}, {inputs: []float64{0, 1}, answer: 1}, {inputs: []float64{1, 1}, answer: 1}}},
		{desc: "NAND回路", trains: []train{{inputs: []float64{0, 0}, answer: 1}, {inputs: []float64{1, 0}, answer: 1}, {inputs: []float64{0, 1}, answer: 1}, {inputs: []float64{1, 1}, answer: 0}}},
		{desc: "NOR回路", trains: []train{{inputs: []float64{0, 0}, answer: 1}, {inputs: []float64{1, 0}, answer: 0}, {inputs: []float64{0, 1}, answer: 0}, {inputs: []float64{1, 1}, answer: 0}}},
	}

	for _, test := range testTable {
		test := test
		t.Run(test.desc, func(t *testing.T) {
			t.Parallel()

			sp := NewSP(2, 0.1, time.Now().UnixNano())

			// 100回学習する 4種類なので25回ずつ
			for i := 0; i < 100; i++ {
				err := sp.Learn(test.trains[i%4].inputs, test.trains[i%4].answer)
				if err != nil {
					t.Fatalf("%s 失敗\nエラー: %v\n", t.Name(), err)
				}
			}

			for _, train := range test.trains {
				ans, err := sp.Output(train.inputs)
				if train.answer != ans || err != nil {
					t.Fatalf("%s 失敗\n期待: %f, 実際: %f\nエラー: %v\n", t.Name(), train.answer, ans, err)
				}
			}
		})
	}
}

func TestSP_E(t *testing.T) {
	t.Parallel()
	testTable := []struct {
		desc   string
		output float64
		answer float64
		expect float64
	}{
		{desc: "出力が1で正答が0なら-1", output: 1, answer: 0, expect: -1},
		{desc: "出力が1で正答が0.5なら-0.5", output: 1, answer: 0.5, expect: -0.5},
		{desc: "出力が1で正答が1なら0", output: 1, answer: 1, expect: 0},
		{desc: "出力が0で正答が0なら0", output: 0, answer: 0, expect: 0},
		{desc: "出力が0で正答が0.5なら0.5", output: 0, answer: 0.5, expect: 0.5},
		{desc: "出力が0で正答が1なら1", output: 0, answer: 1, expect: 1},
	}

	for _, test := range testTable {
		test := test
		t.Run(test.desc, func(t *testing.T) {
			t.Parallel()
			sp := new(SP)
			actual := sp.E(test.output, test.answer)
			if test.expect != actual {
				t.Fatalf("%s 失敗\n期待: %f, 実際: %f\n", t.Name(), test.expect, actual)
			}
		})
	}
}

func TestSP_A(t *testing.T) {
	t.Parallel()
	testTable := []struct {
		desc   string
		output float64
		expect float64
	}{
		{desc: "入力が1なら1", output: 1, expect: 1},
		{desc: "入力が0.1なら1", output: 0.1, expect: 1},
		{desc: "入力が0.01なら1", output: 0.01, expect: 1},
		{desc: "入力が0なら0", output: 0, expect: 0},
		{desc: "入力が-0.01なら0", output: -0.01, expect: 0},
		{desc: "入力が-0.1なら0", output: -0.1, expect: 0},
		{desc: "入力が-1なら0", output: -1, expect: 0},
	}

	for _, test := range testTable {
		test := test
		t.Run(test.desc, func(t *testing.T) {
			t.Parallel()
			sp := new(SP)
			actual := sp.A(test.output)
			if test.expect != actual {
				t.Fatalf("%s 失敗\n期待: %f, 実際: %f\n", t.Name(), test.expect, actual)
			}
		})
	}
}
